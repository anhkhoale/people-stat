const fs = require('fs');
const axios = require('axios');


const generateData = async (users) => {

    let uri = "https://randomuser.me/api/"
    let { data } = await axios.get(uri, {
        params: {
            results: users
        }
    });

    fs.writeFileSync('database.json', JSON.stringify(data, null, 4));
}

generateData(5000);