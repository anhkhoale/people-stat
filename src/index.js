import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import Profile from './components/Profile';
import LoginForm from './components/LoginForm';
import userReducer from './features/user';

const store = configureStore ({
  reducer: {
    user: userReducer,
  }
})

const Routing = () => {
  return (
    <Router>
      <Routes>
          <Route path='/profile' element={<Profile />} />
          <Route path='/login' element={<LoginForm/>} />
      </Routes>
    </Router>
  );
}


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Routing />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
