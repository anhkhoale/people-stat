import {React, useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useNavigate  } from "react-router-dom";
import photoProfile from '../img/photo-profil.png';
import { useSelector } from 'react-redux';

function Profile({ authorized }) {
  let navigate = useNavigate();
  const user = useSelector((state) => state.user.value);
  const [dateState, setDateState] = useState(new Date());
  useEffect(() => {
         setInterval(() => setDateState(new Date()), 30000);
  }, []);

  const Logout = () => {
    navigate('/login', {replace: true});
  }

  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
      <span class="navbar-brand">
        <img src={photoProfile} width="30" alt='' height="30" class="d-inline-block align-text-top"/>
        Profile {user.name}
      </span>
      <p class="d-inline-block align-text-top">
        {dateState.toLocaleString('en-US', {
          hour: 'numeric',
          minute: 'numeric',
          hour12: true,
      })}
      </p>
        <Button variant="primary" type="submit" onClick={Logout}>Log out</Button>
      </div>
    </nav>
  );
}

export default Profile