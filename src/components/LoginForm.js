import React, { useState } from 'react';
import { Form, FormControl, Button, Container } from 'react-bootstrap';
import '../style.css'
import { useNavigate  } from "react-router-dom";
import { useDispatch, userDispatch } from "react-redux";
import { login } from "../features/user"

function LoginIn() {
  const dispatch = useDispatch();
}

function LoginForm() {
  const adminUser = {
    email: "admin@admin.com",
    password: "admin123"
  }

  const [error, setError] = useState("");
  let navigate = useNavigate();

  const goToProfile = id => {
    navigate('/profile', {replace: true});
  }

  const Login = details => {
    if((details.email === adminUser.email) && (details.password === adminUser.password)) {
      goToProfile();
    } else {
      setError("Wrong info");
    }
  }
  const [details, setDetails] = useState({name: "", email: "", password: ""});
  
  async function submitHandler(e) {
    e.preventDefault();
    Login(details);
  }
  
  return (
    <Container style={{width: '20%', marginTop: '20px'}}>
      <Form className='center-form border border-secondary p-3 mt-20 ml-20' onSubmit={submitHandler}>
        <h2>Login</h2>
        {(error !== "") ? (<div className='error'>{error}</div>) : ""}
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email: </Form.Label>
          <Form.Control className="w-20" type="email" placeholder="Enter email" onChange={e => setDetails({...details, email: e.target.value})} value={details.email}/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" onChange={e => setDetails({...details, password: e.target.value})} value={details.password}/>
        </Form.Group>
        <Button variant="primary" type='submit'>
          Login
        </Button>
      </Form>
    </Container>
  )
}

export default LoginForm